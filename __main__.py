#!/usr/bin/env python3.6

import socket
import threading

from utils.configs_objects import ServerConfig
from utils.bitrek_protocol import Bitrek


class Server:
    """
    Class 'Server' is main class in this program
    Listen trackers & write received data
    """

    def __init__(self):
        """
        Initialization object type 'Server'
        Creating socket & other
        """

        config = ServerConfig()

        self.host = config.host
        self.port = int(config.port)

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        """
        Listen new connection & start new thread for connection

        :return: None
        """

        self.sock.listen(100)

        while True:
            client, address = self.sock.accept()
            print('New connection:', client)

            threading.Thread(target=self.listen_client, args=(client, address)).start()

    @staticmethod
    def listen_client(client: socket.socket, address: tuple):
        """
        Create new tracker object
        Listen client for receive data

        :param client: socket, client socket for work with tracker
        :param address: tuple, host & port of client
        :return: None
        """

        new_tracker = Bitrek(client, address)
        new_tracker.listen()


if __name__ == '__main__':

    server = Server()
    server.listen()
