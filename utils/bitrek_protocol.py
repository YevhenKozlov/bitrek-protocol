#!/usr/bin/env python3.6

import time
import socket

from utils.parser import Parser


class Bitrek:
    """
    Class 'Bitrek'
    Listen data for tracker
    """

    def __init__(self, client: socket.socket, address: tuple):
        """
        Initialization new tracker object

        :param client: socket, client socket for work with tracker
        :param address: tuple, host & port of client
        """

        self.client = client
        self.address = address
        self.imei = None

    @staticmethod
    def checksum(data: str) -> bool:
        """
        Checksum calculation

        :param data: str, all received data from tracker
        :return: bool, return 'True' if OK else - 'False'
        """

        size_bytes = int(data[8:16], base=16)
        data_bytes = data[16:-8]

        crc16 = int(data[-8:], base=16)

        try:
            crc16_result = 0
            for i in range(0, size_bytes * 2, 2):

                temp_value = int(data_bytes[i] + data_bytes[i + 1], base=16)
                crc16_result ^= temp_value

                for j in range(8):
                    crc16_result = (crc16_result >> 1) ^ 0xA001 if crc16_result & 0x0001 else crc16_result >> 1

            return crc16_result == crc16

        except:
            return False

    def handshake(self) -> bool:
        """
        Handshake with tracker

        :return: bool, result of handshake
        """

        print(f'Hi, tracker {self.imei}!')
        self.client.sendall(b'\x01')

        return True

    def send_true(self):
        """
        Send 1 to tracker for success

        :return: None
        """

        self.client.sendall(b'\x00\x00\x00\x01')

    def send_false(self):
        """
        Send 0 to tracker for fail

        :return: None
        """

        self.client.sendall(b'\x00\x00\x00\x00')

    def listen(self):
        """
        Listen bitrek data

        :return: None
        """

        try:
            self.client.settimeout(180)
            max_data_size = 2 ** 16
            data = self.client.recv(max_data_size)

            self.imei = data.decode()[2:]

            if self.handshake():
                while True:

                    data = self.client.recv(max_data_size)
                    if not data:
                        continue

                    data_hex = data.hex()
                    timestamp = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(time.time()))
                    print(f'[{timestamp}, {self.imei}] - {data_hex}')

                    if self.checksum(data_hex):
                        print(f'[{timestamp}, {self.imei}] - Checksum successful.')
                        self.send_true()

                        # Parsing received data:
                        parser_obj = Parser(self.imei, data_hex)
                        print(f'[{timestamp}, {self.imei}] - {parser_obj.data}')

                    else:
                        print(f'[{timestamp}, {self.imei}] - Checksum failed.')
                        self.send_false()

            else:
                raise Exception('handshake failed')

        except Exception as e:
            print(f'Goodbye, {self.imei}, because: "{e}"')

        finally:
            self.client.close()
