#!/usr/bin/env python3.6

import time

from utils.json_object import json_obj, package_obj, sensor_obj
from utils.redis_connection import connect_to_redis


class Parser:
    """
    Data parser for Bitrek protocol
    """

    def __init__(self, imei: str, data: str):
        """
        Initialization Parser-object
        Parsing data, creating Redis connect

        :param imei: str, imei current car
        :param data: str, received hex data
        """

        # init redis connect
        self.__redis_db = connect_to_redis()

        # init
        self.data = json_obj.copy()
        self.data['packages'] = list()

        # parsing
        self.data['imei'] = imei
        self.data['raw_data'] = data
        self.data['timestamp'] = time.time()
        self.data['prefix'] = data[:8]
        self.data['size'] = int(data[8:16], base=16)
        self.data['codec'] = data[16:18]
        self.data['crc_16'] = data[-8:]
        self.data['number_of_packages'] = int(data[18:20], base=16)

        # remove read data
        packages = data[20:-10]

        for i in range(int(data[18:20], base=16)):

            # init
            package = package_obj.copy()
            package['number_of_sensors_type'] = list()
            package['sensors'] = list()

            # parsing
            package['timestamp'] = int(packages[:16], base=16) / 10 ** 3
            package['priority'] = int(packages[16:18], base=16)
            package['latitude'] = int(packages[18:26], base=16) / 10 ** 7
            package['longitude'] = int(packages[26:34], base=16) / 10 ** 7
            package['altitude'] = int(packages[34:38], base=16)
            package['azimuth'] = int(packages[38:42], base=16)
            package['number_of_satellite'] = int(packages[42:44], base=16)
            package['speed'] = int(packages[44:48], base=16)
            package['action'] = int(packages[48:50], base=16)
            package['number_of_sensors'] = int(packages[50:52], base=16)

            # remove read data
            packages = packages[52:]

            for byte in [1, 2, 4, 8]:

                # parsing
                count_bytes = int(packages[:2], base=16)

                # remove read data
                packages = packages[2:]

                package['number_of_sensors_type'].append(count_bytes)

                for number in range(count_bytes):

                    # init
                    new_sensor = sensor_obj.copy()

                    # parsing
                    new_sensor['type'] = byte
                    new_sensor['id'] = packages[:2]
                    new_sensor['data'] = packages[2:2 + 2 * byte]

                    # remove read data
                    packages = packages[2 + 2 * byte:]

                    # save
                    package['sensors'].append(new_sensor)

            # save
            self.data['packages'].append(package)

    def save_to_redis(self):
        """
        Save received data to Redis

        :return: None
        """

        pass
