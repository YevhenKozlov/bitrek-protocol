#!/usr/bin/env python3.6

import redis

from utils.configs_objects import RedisConfig


def connect_to_redis():
    """
    Connecting to Redis database

    :return: redis.Redis, database object
    """

    config = RedisConfig()
    return redis.Redis(host=config.host, port=int(config.port), db=int(config.database), password=config.password)
