#!/usr/bin/env python3.6

"""
This package contains main json object for data parser
"""

json_obj = {
    'imei':                     None,       # OK    # imei current tracker (str)
    'raw_data':                 None,       # OK    # raw received data (str)
    'prefix':                   None,       # OK    # prefix data (str)
    'size':                     None,       # OK    # size of data bytes (int)
    'codec':                    None,       # OK    # codec data (str)
    'crc_16':                   None,       # OK    # crc-16 data (str)
    'number_of_packages':       None,       # OK    # number of 'package_obj' (int)
    'packages':                 None,       # OK    # list of 'package_obj' (list)
    'timestamp':                None,       # OK    # time create json (float)
}

package_obj = {
    'priority':                 None,       # OK    # data priority (int)
    'latitude':                 None,       # OK    # latitude (float)
    'longitude':                None,       # OK    # longitude (float)
    'altitude':                 None,       # OK    # altitude (int)
    'azimuth':                  None,       # OK    # angle (int)
    'speed':                    None,       # OK    # speed of car (int)
    'number_of_satellite':      None,       # OK    # number of satellite (int)
    'action':                   None,       # OK    # data created on event (int)
    'number_of_sensors':        None,       # OK    # number of 'sensor_obj' (int)
    'number_of_sensors_type':   None,       # OK    # number of sensors concrete type (list), for example: [0, 1, 5, 3]
    'sensors':                  None,       # OK    # list of 'sensor_obj' (list)
    'timestamp':                None,       # OK    # time create this data (float)
}

sensor_obj = {
    'id':                       None,       # OK    # id current sensor (str)
    'type':                     None,       # OK    # type, number of bytes (int)
    'data':                     None,       # OK    # data of this sensor (str)
}
