#!/usr/bin/env python3.6

from abc import ABC
from configparser import ConfigParser


class Config(ABC):
    """
    Abstract config class
    Encapsulation working with configs
    """

    def __setattr__(self, key: str, value: str):
        """
        Setter for update option configs
        Not create new option if not exist, only update existing

        :param key: str, option name
        :param value: str, data
        :return: None
        """

        # exception if type not string
        if type(value) != str:
            raise TypeError('Type must be str!')

        config = ConfigParser()
        config.read(self.path)

        # exception if not exist
        config[self.section][key]

        config.set(self.section, key, value)

        with open(self.path, 'w') as config_file:
            config.write(config_file)

    def __getattr__(self, key: str) -> str:
        """
        Return option data

        :param key: str, option name
        :return: str, data from this option
        """

        config = ConfigParser()
        config.read(self.path)

        return config[self.section][key]

    def get_keys(self) -> list:
        """
        Getting all keys from selected config

        :return: list, collection keys from config
        """

        config = ConfigParser()
        config.read(self.path)

        return [item[0] for item in config.items(self.section)]


class ServerConfig(Config):
    """
    Class 'ServerConfig'

    Options:
        self.host - str, example 'localhost' or '0.0.0.0', etc...
        self.port - str, port {80 ... 65536}

    Hints in PyCharm not worked
    """

    path = 'configs/server.ini'
    section = 'MAIN'


class RedisConfig(Config):
    """
    Class 'RedisConfig'

    Options:
        self.host - str, example 'localhost' or '0.0.0.0', etc...
        self.port - str, port {80 ... 65536}
        self.database - str, number of database in Redis
        self.password - str, password for auth

    Hints in PyCharm not worked
    """

    path = 'configs/redis.ini'
    section = 'CONNECTION'


class DatabaseConfig(Config):
    """
    Class 'DatabaseConfig'

    Options:
        self.host - str, example 'localhost' or '0.0.0.0', etc...
        self.port - str, port {80 ... 65536}
        self.database - str, name of database
        self.user - str, login
        self.password - str, user password

    Hints in PyCharm not worked
    """

    path = 'configs/database.ini'
    section = 'CONNECTION'


class GeliosConfig(Config):
    """
    Class 'GeliosConfig'

    Options:
        self.host - str, example 'localhost' or '0.0.0.0', etc...
        self.port - str, port {80 ... 65536}

    Hints in PyCharm not worked
    """

    path = 'configs/gelios.ini'
    section = 'CONNECTION'
