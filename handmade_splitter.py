data = input('Введите строку с данными: ')

print('\n* * *\n')
print('Преамбула:', data[:8])
print('Длина данных:', data[8:16], '=', int(data[8:16], base=16))
print('Кодек:', data[16:18])
print('Количество пакетов (в начале):', data[18:20], '=', int(data[18:20], base=16))
print('Количество пакетов (в конце):', data[-10:-8], '=', int(data[-10:-8], base=16))
print('CRC16 (контроль):', data[-8:])

package = data[20:-10]

for i in range(int(data[18:20], base=16)):

    print('\n* * *\n')
    print(f'Пакет №{i}')
    print('Время:', package[:16], '=', int(package[:16], base=16) / 1000)
    print('Приоритет:', package[16:18], '=', int(package[16:18], base=16))
    print('Долгота:', package[18:26], '=', int(package[18:26], base=16) / 10000000)
    print('Широта:', package[26:34], '=', int(package[26:34], base=16) / 10000000)
    print('Высота:', package[34:38], '=', int(package[34:38], base=16))
    print('Азимут:', package[38:42], '=', int(package[38:42], base=16))
    print('Спутники:', package[42:44], '=', int(package[42:44], base=16))
    print('Скорость:', package[44:48], '=', int(package[44:48], base=16))
    print('Событийность:', package[48:50], '=', int(package[48:50], base=16))
    print('Общее кол. I/O данных:', package[50:52], '=', int(package[50:52], base=16))

    package = package[52:]

    for byte in [1, 2, 4, 8]:
        count_bytes = int(package[:2], base=16)
        package = package[2:]

        for number in range(count_bytes):
            print(f'Датчик ID[{byte}, {number}]:', package[:2])
            print(f'Данные датчика ID[{byte}, {number}]:', package[2:2 + 2 * byte])
            package = package[2 + 2 * byte:]
